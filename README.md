# Szachy

Projekt wykonany na podstawie zadania:

> Na tradycyjnej szachownicy 8x8 w jaki sposób rozstawić 8 hetmanów,
> aby wzajemnie się nie atakowały?
> Ile jest takich rozwiązań i ile jest rozwiązań symetrycznych?

## Co potrzebujemy:
* [CodeBlocks + MinGW](http://www.codeblocks.org/downloads)
* [Git](https://git-scm.com/downloads) (opcjonalnie)

## Jak pobrać i uruchomić?

1. Pobieramy paczkę zip z [LINK](https://gitlab.com/KassVaru/szachy/-/archive/master/szachy-master.zip)
2. Uruchamiamy program CodeBlocks
3. Wypakowujemy i importujemy nasz projekt do CodeBlocks
4. Wciskamy klawisz F8 na klawiaturze
5. Korzystamy z cudowności programu

### Opcjonalnie
Możemy również wykorzystać pakiet GIT, by pobrać najnowszą wersję programu:
1. W konsoli git/cmd wpisujemy: ``` git clone https://gitlab.com/KassVaru/szachy.git ```
2. Reszta taka sama jak wyżej