#include <iostream>
#include <locale.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>
#include <fstream>

using namespace std;

int c[8][8];
char alph[] = "ABCDEFGH";

// Check is board full
int is_full(){
    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
            if(c[i][j]==0){
                return 0;
            }
        }
    }
    return 1;
}

// Send last coords to file
void send_to_file(int x,int y){
    fstream file;
    file.open("test.txt", ios::out | ios::app);
    file<<"x: "<<x<<endl;
    file<<"y: "<<y<<endl;
    file.close();
}

// Prawo-dół
void p_d(int x, int y){
    int i=1;
    while((x+i<=7) && (y+i<=7)){
        c[y+i][x+i]=1;
        i++;
    }
}

// Prawo-góra
void p_g(int x, int y){
    int i=1;
    while((x+i<=7) && (y-i>=0)){
        c[y-i][x+i]=1;
        i++;
    }
}

// Lewo-dół
void l_d(int x, int y){
    int i=1;
    while((x-i>=0) && (y+i<=7)){
        c[y+i][x-i]=1;
        i++;
    }
}

// Lewo-góra
void l_g(int x, int y){
    int i=1;
    while((x-i>=0) && (y-i>=0)){
        c[y-i][x-i]=1;
        i++;
    }
}

// Wszystkie na ukos
void all(int x, int y){
    l_d(x,y);
    l_g(x,y);
    p_g(x,y);
    p_d(x,y);
}

// Wyszukiwanie i wypisywanie miejsc bitych po ukosie
void where(int x,int y){
    for(int i=0; i<8; i++){ // Poziomo - Pionowo
        c[i][x] = 1;
        c[y][i] = 1;
    }

    switch(x){
    case 0:
        switch(y){
        case 0:
            p_d(x,y);
            break;
        case 7:
            p_g(x,y);
            break;
        default:
            p_d(x,y);
            p_g(x,y);
            break;
        }
        break;
    case 7:
        switch(y){
        case 0:
            l_d(x,y);
            break;
        case 7:
            l_g(x,y);
            break;
        default:
            l_d(x,y);
            l_g(x,y);
            break;
        }
        break;
    default:
        switch(y){
        case 0:
            switch(x){
            case 0:
                p_d(x,y);
                break;
            case 7:
                l_d(x,y);
                break;
            default:
                p_d(x,y);
                l_d(x,y);
                break;
            }
            break;
        case 7:
            switch(x){
            case 0:
                p_g(x,y);
                break;
            case 7:
                l_g(x,y);
                break;
            default:
                p_g(x,y);
                l_g(x,y);
                break;
            }
            break;
        default:
            all(x,y);
            break;
        }
        break;
    }
}

// Convert int coords to char coords
char to_alph(int a){
    a=alph[a];
    return a;
}

// Wyświetlenie planszy
void show(int x,int y){
    cout<<x+1<<"  "<<to_alph(y)<<endl<<endl<<"   ";
    c[y][x]=5;
    for(int i=1;i<9;i++){
        cout<<i<<"  ";
    }
    cout<<endl;
    for(int i=0;i<8;i++){
            cout<<alph[i]<<"  ";
        for(int j=0;j<8;j++){
            cout<<c[i][j]<<"  ";
        }
        cout<<endl;
    }
    cout<<endl<<endl;
}

// Tworzenie reszty hetman'ów
int make_more(){
    int r,s,l=1;
    while(l<9){
        r=rand()%8;
        s=rand()%8;
        if(c[s][r]==0){
            send_to_file(r,s);
            l++;
            where(r,s);
            show(r,s);
        }
        if(is_full()==1){
            return l;
        }
    }
}

int main()
{
    setlocale( LC_CTYPE, "Polish" );
    srand(time(NULL));
    int x,y;

    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
            c[i][j]=0;
            //cout<<c[i][j]<<"  ";
        }
        //cout<<endl;
    }
    //cout<<endl;

    x = rand()%8;
    y = rand()%8;
    send_to_file(x,y);
    where(x,y);
    show(x,y);

    // Tworzenie kolejnych hetman'ów
    cout<<"Udalo sie stworzyc: "<<make_more()<<" hetmanow.";
    system("PAUSE");
    return 0;
}
